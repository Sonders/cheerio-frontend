import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class UserProfile extends Component {
    render() {
        return (
            <div className="profile">
                <div className="profile-header" style={{ backgroundImage: "url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/r8K5nG9Pxiyh9wvn6/videoblocks-abstract-geometric-background-with-moving-lines-dots-and-triangles-plexus-fantasy-abstract-technology_s_rx2tgz_thumbnail-full01.png')"}}>
                    <div className="profile-card">
                        <div className="user-avatar" style={{ backgroundImage: "url('/assets/images/avatar.jpg')" }}></div>
                        <div className="user-info">
                            <div className="user-name">Лобыкин Юрий Вячеславович</div>

                            <div className="user-status">Спасибо что посетил (а) мою страницу, я рад тогда, когда нахожу
                                единомышленников!
                            </div>

                            <div className="user-description">
                                Звание: Заслуженный критик<br/>
                                Предпочтение: Мясо, много мяса!
                            </div>
                        </div>
                    </div>
                </div>

                <div className="profile-footer">
                    <Link to="#" className="subscribe">Подписаться</Link>
                    <Link to="#" className="chat"></Link>
                    <span className="subscribers-amount">1300 подписчиков</span>
                    <span className="reviews-amount">1300 рецензий</span>
                </div>
            </div>
        )
    }
}

export default UserProfile