import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class UserNewsFeedStocks extends Component {
    render() {
        return (
            <div className="news-feed-stocks">
                <div className="stocks-start">
                    <span></span>
                </div>

                <Link to="#" className="stock" style={{ backgroundImage: "url('/assets/images/история 1.jpg')" }}>
                    <span style={{ backgroundImage: "url('/assets/images/avatar.jpg')" }}></span>
                </Link>

                <Link to="#" className="stock" style={{ backgroundImage: "url('/assets/images/История 2.jpg')" }}>
                    <span style={{ backgroundImage: "url('/assets/images/selera-logo.jpg')" }}></span>
                </Link>

                <Link to="#" className="stock" style={{ backgroundImage: "url('/assets/images/История 3.jpg')" }}>
                    <span style={{ backgroundImage: "url('/assets/images/selera-logo.jpg')" }}></span>
                </Link>

                <Link to="#" className="stock" style={{ backgroundImage: "url('/assets/images/История 4.jpg')" }}>
                    <span style={{ backgroundImage: "url('/assets/images/selera-logo.jpg')" }}></span>
                </Link>
            </div>
        )
    }
}

export default UserNewsFeedStocks