import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class UserNewsFeedCategories extends Component {
    render() {
        return (
            <div className="news-feed-categories">
                <ul className="items">
                    {
                        this.props.items.map((item, index) => {
                            return <li key={index} className="item"><NavLink exact to={item.path} activeClassName="active">{item.name}</NavLink></li>
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default UserNewsFeedCategories