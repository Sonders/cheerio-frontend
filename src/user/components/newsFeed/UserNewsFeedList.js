import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class UserNewsFeedList extends Component {
    render() {
        return (
            <div className="news-feed-list">
                <div className="news-item">
                    <div className="news-header">
                        <div className="news-info">
                            <Link to="#" className="user-avatar"
                               style={{ backgroundImage: "url('/assets/images/avatar.jpg')" }}></Link>

                            <div className="user-description">
                                <Link to="#" className="user-name">Лобыкин Ю. В.</Link>
                                <span className="news-type">Рецензия</span>
                            </div>

                            <div className="review-arrow"></div>

                            <Link to="#" className="establishment-avatar"
                               style={{ backgroundImage: "url('/assets/images/selera-logo.jpg')" }}></Link>

                            <div className="establishment-description">
                                <Link to="#" className="establishment-name">Selera Meneer</Link>
                                <span className="establishment-type">Ресторан</span>
                            </div>

                            <div className="rating">= 4.7</div>
                        </div>

                        <div className="news-options"></div>
                    </div>

                    <div className="news-content">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                        took a galley of type and scrambled it to make a type specimen book. It has survived not only
                        five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                        passages, and more recently with desktop publishing software like Aldus PageMaker including
                        versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                        survived not only five centuries, but also the leap into electronic typesetting, remaining
                        essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                        containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                        PageMaker including versions of Lorem Ipsum.
                    </div>

                    <div className="news-actions">
                        <Link to="#" className="unlike"></Link>
                        <span className="news-rating">593</span>
                        <Link to="#" className="like"></Link>

                        <span className="comments"></span>
                        <span className="comments-amount">12</span>
                    </div>
                </div>

                <div className="news-item">
                    <div className="news-header">
                        <div className="news-info">
                            <Link to="#" className="user-avatar"
                               style={{ backgroundImage: "url('/assets/images/avatar.jpg')" }}></Link>

                            <div className="user-description">
                                <Link to="#" className="user-name">Лобыкин Ю. В.</Link>
                                <span className="news-type">Новость</span>
                            </div>
                        </div>

                        <div className="news-options"></div>
                    </div>

                    <div className="news-content">
                        <Link to="#" className="news-image" style={{ backgroundImage: "url('/assets/images/История 3.jpg')" }}></Link>

                        <div className="news-text">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting
                            industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when
                            an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                            has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                            software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy
                            text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                            dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap
                            into electronic typesetting, remaining essentially unchanged. It was popularised in the
                            1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently
                            with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </div>
                    </div>

                    <div className="news-actions">
                        <Link to="#" className="unlike"></Link>
                        <span className="news-rating">593</span>
                        <Link to="#" className="like"></Link>

                        <span className="comments"></span>
                        <span className="comments-amount">12</span>

                        <span className="shares"></span>
                        <span className="shares-amount">1900</span>
                    </div>
                </div>

                <div className="news-item">
                    <div className="news-header">
                        <div className="news-info">
                            <Link to="#" className="user-avatar"
                               style={{ backgroundImage: "url('/assets/images/avatar.jpg')" }}></Link>

                            <div className="user-description">
                                <Link to="#" className="user-name">Лобыкин Ю. В.</Link>
                                <span className="news-type">Рецензия</span>
                            </div>

                            <div className="share-arrow"></div>

                            <Link to="#" className="establishment-avatar"
                               style={{ backgroundImage: "url('/assets/images/selera-logo.jpg')" }}></Link>

                            <div className="establishment-description">
                                <Link to="#" className="establishment-name">Selera Meneer</Link>
                                <span className="establishment-type">Ресторан</span>
                            </div>
                        </div>

                        <div className="news-options"></div>
                    </div>

                    <div className="news-content">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                        took a galley of type and scrambled it to make a type specimen book. It has survived not only
                        five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                        passages, and more recently with desktop publishing software like Aldus PageMaker including
                        versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                        survived not only five centuries, but also the leap into electronic typesetting, remaining
                        essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                        containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                        PageMaker including versions of Lorem Ipsum.
                    </div>

                    <div className="news-actions">
                        <Link to="#" className="unlike"></Link>
                        <span className="news-rating">593</span>
                        <Link to="#" className="like"></Link>

                        <span className="comments"></span>
                        <span className="comments-amount">12</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserNewsFeedList