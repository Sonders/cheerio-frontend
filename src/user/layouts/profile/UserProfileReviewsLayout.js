import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import UserProfileLayout from './UserProfileLayout'
import UserNewsFeedList from '../../components/newsFeed/UserNewsFeedList'

class UserProfileReviewsLayout extends Component {
    render() {
        return (
            <UserProfileLayout content={
                <UserNewsFeedList />
            } />
        )
    }
}

export default UserProfileReviewsLayout