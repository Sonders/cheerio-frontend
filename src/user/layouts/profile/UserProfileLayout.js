import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import Layout from '../../../main/layouts/layout'

import UserProfile from '../../components/profile/UserProfile'

import './style.css'
import UserNewsFeedCategories from "../../components/newsFeed/UserNewsFeedCategories";
import UserNewsFeedStocks from "../../components/newsFeed/UserNewsFeedStocks";

class UserProfileLayout extends Component {
    constructor(props) {
        super(props)

        this.items = [
            {
                path: "/profile",
                name: "Лента"
            },
            {
                path: "/profile/reviews",
                name: "Рецензии"
            },
            {
                path: "/profile/comments",
                name: "Комментарии"
            },
            {
                path: "/profile/favorites",
                name: "Рекомендую"
            }
        ];
    }

    render() {
        return (
            <Layout content={
                <div>
                    <UserProfile />
                    <div className="news-feed user-profile">
                        <UserNewsFeedCategories items={this.items}/>
                        <div className="news-feed-main inner">
                            <UserNewsFeedStocks />
                            {this.props.content}
                        </div>
                    </div>
                </div>
            }/>
        )
    }
}

export default UserProfileLayout