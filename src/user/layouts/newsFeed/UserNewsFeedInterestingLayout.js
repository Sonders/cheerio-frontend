import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import UserNewsFeedLayout from './UserNewsFeedLayout'
import UserNewsFeedStocks from "../../components/newsFeed/UserNewsFeedStocks";
import UserNewsFeedList from "../../components/newsFeed/UserNewsFeedList";

class UserNewsFeedInterestingLayout extends Component {
    render() {
        return (
            <UserNewsFeedLayout content={
                <div>
                    <UserNewsFeedStocks />
                    <UserNewsFeedList />
                </div>
            } />
        )
    }
}

export default UserNewsFeedInterestingLayout