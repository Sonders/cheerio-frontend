import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import Layout from '../../../main/layouts/layout'
import UserNewsFeedCategories from '../../components/newsFeed/UserNewsFeedCategories'

import './style.css'

class UserNewsFeedLayout extends Component {
    constructor(props) {
        super(props)

        this.items = [
            {
                path: "/feed",
                name: "Вся лента"
            },
            {
                path: "/feed/interesting",
                name: "Интересное"
            },
            {
                path: "/feed/new",
                name: "Свежее"
            },
            {
                path: "/feed/discussed",
                name: "Обсуждаемое"
            }
        ];
    }

    render() {
        return (
            <Layout content={
                <div className="news-feed">
                    <UserNewsFeedCategories items={this.items} />
                    <div className="news-feed-main inner">
                        {this.props.content}
                    </div>
                </div>
            } />
        )
    }
}

export default UserNewsFeedLayout