import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import Layout from '../../../main/layouts/layout'

import './style.css'

class UserSubscriptionsLayout extends Component {
    render() {
        return (
            <Layout content={
                <div className="block inner">Мои подписки</div>
            }/>
        )
    }
}

export default UserSubscriptionsLayout