import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import Layout from '../../../main/layouts/layout'

import './style.css'

class EstablishmentRatingLayout extends Component {
    render() {
        return (
            <Layout content={
                <div className="block inner">Топ заведений</div>
            }/>
        )
    }
}

export default EstablishmentRatingLayout