import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="container">
                    <div className="copyright">© 2019 Cheerio. Все права защищены</div>
                </div>
            </div>
        )
    }
}

export default Footer