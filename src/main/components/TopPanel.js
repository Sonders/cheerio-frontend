import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class TopPanel extends Component {
    render() {
        return <div className="panel">
            <div className="container">
                <Link to="/feed" className="logo"></Link>

                <div className="tools">
                    <div className="search">
                        <span className="search-icon"></span>
                        <input type="text" placeholder="Поиск"/>
                    </div>

                    <div className="user">
                        <Link to="#" className="user-balance">1500</Link>
                        <Link to="/profile" className="user-name">Лобыкин Ю. В.</Link>
                        <Link to="/profile" className="user-avatar" style={{ backgroundImage: "url('/assets/images/no-image.png')" }}></Link>
                        <Link to="#" className="user-options"></Link>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default TopPanel