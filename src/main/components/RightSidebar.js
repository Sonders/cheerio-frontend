import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class RightSidebar extends Component {
    render() {
        return (
            <div className="right-sidebar">
                <div className="recommend-establishment"
                     style={{backgroundImage: "linear-gradient(to bottom, black, rgba(0, 0, 0, 0.47)), url('/assets/images/Ресторан 2.jpg')"}}>
                    <div className="establishment-name">
                        Ресторан <br/> <span>Selera Meneer</span>
                    </div>

                    <div className="establishment-description">
                        -Место для проведения банкетов <br/>
                        -Приятная атмосфера <br/>
                        -Живая музыка <br/>
                        -Вкусные блюда <br/>
                        -Достойное обслуживание
                    </div>

                    <Link to="#" className="more">Подробнее</Link>
                </div>
            </div>
        )
    }
}

export default RightSidebar