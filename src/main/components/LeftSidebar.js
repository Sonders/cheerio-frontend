import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class LeftSidebar extends Component {
    render() {
        return (
            <div className="left-sidebar">
                <div className="main-menu">
                    <ul className="items">
                        <li className="item"><NavLink to="/profile" activeClassName="active">Моя страница</NavLink></li>
                        <li className="item"><NavLink to="/feed" activeClassName="active">Лента новостей</NavLink></li>
                        <li className="item"><NavLink to="/orders" activeClassName="active">Мои заказы</NavLink></li>
                        <li className="item"><NavLink to="/subscriptions" activeClassName="active">Мои подписки</NavLink></li>
                        <li className="item"><NavLink to="/rating" activeClassName="active">Топ заведений</NavLink></li>
                        <li className="item"><NavLink to="/advertising" activeClassName="active">По рекламе</NavLink></li>
                        <li className="item"><NavLink to="/about" activeClassName="active">О нас</NavLink></li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default LeftSidebar