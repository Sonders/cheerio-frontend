import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import TopPanel from '../components/TopPanel'
import LeftSidebar from '../components/LeftSidebar'
import RightSidebar from '../components/RightSidebar'
import Footer from '../components/Footer'

class Layout extends Component {
    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <div className="wrapper">
                <div className="header">
                    <TopPanel />
                </div>

                <div className="main container">
                    <LeftSidebar />
                    <div className="content">
                        {this.props.content}
                    </div>
                    <RightSidebar />
                </div>

                <Footer />
            </div>
        )
    }
}

export default Layout