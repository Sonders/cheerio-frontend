import React, { Component } from 'react';
import { Redirect, Switch, BrowserRouter, Route } from 'react-router-dom'

import UserNewsFeedAllLayout from './user/layouts/newsFeed/UserNewsFeedAllLayout'
import UserNewsFeedInterestingLayout from './user/layouts/newsFeed/UserNewsFeedInterestingLayout'
import UserNewsFeedNewLayout from './user/layouts/newsFeed/UserNewsFeedNewLayout'
import UserNewsFeedDiscussedLayout from './user/layouts/newsFeed/UserNewsFeedDiscussedLayout'

import UserOrdersLayout from './user/layouts/orders/UserOrdersLayout'

import UserProfileFeedLayout from './user/layouts/profile/UserProfileFeedLayout'
import UserProfileReviewsLayout from './user/layouts/profile/UserProfileReviewsLayout'
import UserProfileCommentsLayout from './user/layouts/profile/UserProfileCommentsLayout'
import UserProfileFavoritesLayout from './user/layouts/profile/UserProfileFavoritesLayout'

import UserSubscriptionsLayout from './user/layouts/subscriptions/UserSubscriptionsLayout'

import EstablishmentRatingLayout from './user/layouts/rating/EstablishmentRatingLayout'

import EstablishmentMainLayout from './establishment/layouts/EstablishmentMainLayout'
import EstablishmentMenuLayout from './establishment/layouts/EstablishmentMainLayout'
import EstablishmentReviewsLayout from './establishment/layouts/EstablishmentMainLayout'
import EstablishmentContactsLayout from './establishment/layouts/EstablishmentMainLayout'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
            <Route exact path="/" render={() => {
                return <Redirect to="/feed"/>
            }}/>

            <Route exact path="/feed" component={ UserNewsFeedAllLayout }/>
            <Route path="/feed/interesting" component={ UserNewsFeedInterestingLayout }/>
            <Route path="/feed/new" component={ UserNewsFeedNewLayout }/>
            <Route path="/feed/discussed" component={ UserNewsFeedDiscussedLayout }/>

            <Route exact path="/profile" component={ UserProfileFeedLayout }/>
            <Route path="/profile/reviews" component={ UserProfileReviewsLayout }/>
            <Route path="/profile/comments" component={ UserProfileCommentsLayout }/>
            <Route path="/profile/favorites" component={ UserProfileFavoritesLayout }/>

            <Route exact path="/orders" component={ UserOrdersLayout }/>
            <Route exact path="/subscriptions" component={ UserSubscriptionsLayout }/>
            <Route exact path="/rating" component={ EstablishmentRatingLayout }/>
            {/*<Route exact path="/advertising" component={}/>
            <Route exact path="/about" component={}/>

            <Route exact path="/establishments/:id" component={ EstablishmentMainLayout }/>
            <Route path="/establishments/:id/menu" component={ EstablishmentMenuLayout }/>
            <Route path="/establishments/:id/reviews" component={ EstablishmentReviewsLayout }/>
            <Route path="/establishments/:id/contacts" component={ EstablishmentContactsLayout }/>*/ }
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
